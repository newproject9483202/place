"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderStatusService = void 0;
const OrderStatusRepository_1 = require("../repo/OrderStatusRepository");
class OrderStatusService {
    static ServiceFunction(reqst, query) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // Convert the query parameter to lowercase for case-insensitive comparison
                const lowerCaseQuery = query.trim().toLowerCase().replace(/[^a-z]/g, '');
                const customerDto = {
                    customerId: reqst.customerId,
                    orders: reqst.orders
                };
                if (lowerCaseQuery === "placeorder" || lowerCaseQuery === "place_order") {
                    if (reqst.customerId && !reqst.customer) {
                        console.log("customer_id");
                        const customerExist = yield OrderStatusRepository_1.repo.isCustomerExist(reqst, lowerCaseQuery);
                        return customerExist;
                    }
                    else if (reqst.customer && !reqst.customerId) {
                        console.log("customer object");
                        const customerObject = yield OrderStatusRepository_1.repo.isCustomerObject(reqst, lowerCaseQuery);
                        return customerObject;
                    }
                    else {
                        const customerBoth = yield OrderStatusRepository_1.repo.BothCustomerid(reqst, lowerCaseQuery);
                        return customerBoth;
                    }
                }
                else if (lowerCaseQuery === "cancelorder" || lowerCaseQuery === "cancel_order") {
                    const cancelOrder = yield OrderStatusRepository_1.repo.CancelOrder(reqst, lowerCaseQuery);
                    console.log("result at service from repo:", cancelOrder);
                    return cancelOrder;
                }
                else {
                    // Invalid query parameter
                    throw {
                        statusCode: 400,
                        statusMessage: 'Bad Request',
                        result: {
                            response: 'Invalid parameter',
                        },
                    };
                }
            }
            catch (error) {
                throw error;
            }
        });
    }
}
exports.OrderStatusService = OrderStatusService;
