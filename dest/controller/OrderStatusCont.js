"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidateClass = void 0;
const OrderStatusValidate_1 = require("../validator/OrderStatusValidate");
const OrderStatusService_1 = require("../service/OrderStatusService"); // Import the ServiceFunction from OrderStatusService.ts
class ValidateClass {
    static validateOrder(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = req.query.order_type;
                console.log("coming to validate");
                const errors = yield OrderStatusValidate_1.Validator.requestValidation(req.body, query);
                console.log("error response for validator", errors);
                console.log("controller body", req.body);
                console.log("query", req.query.order_type);
                if (errors.length === 0) {
                    const query = req.query.order_type;
                    // If the errors array is empty, call the ServiceFunction in OrderStatusService.ts
                    const serviceresponse = yield OrderStatusService_1.OrderStatusService.ServiceFunction(req.body, query);
                    //res.status(200).send(serviceresponse);
                    res.status(serviceresponse.statusCode).send(serviceresponse);
                }
                else {
                    // If errors are present, throw an error to the front-end
                    res.status(400).json({ error: "Validation errors found", errors: errors });
                }
            }
            catch (error) {
                console.error("Error occurred:", error);
                res.status(500).send({
                    statusCode: 500,
                    Response: error.message || "internal server error"
                });
            }
        });
    }
}
exports.ValidateClass = ValidateClass;
