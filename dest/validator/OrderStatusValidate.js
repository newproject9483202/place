"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Validator = void 0;
//import express, { Request, Response } from 'express';
class Validator {
    static requestValidation(req, query) {
        return __awaiter(this, void 0, void 0, function* () {
            const customerData = req;
            const errors = [];
            //const Orders: Customer = req.body.orders;
            console.log("customerData", customerData);
            //const errors: validateError[] = [];
            const customerIdRegex = /^\d{1,5}$/;
            const validPlaceOrderQueries = ["placeorder", "place_order"];
            const validCancelOrderQueries = ["cancelorder", "cancel_order"];
            // Trim, convert to lowercase, and remove non-alphabetic characters
            const lowerCaseQuery = query.trim().toLowerCase().replace(/[^a-z]/g, '');
            if (validPlaceOrderQueries.includes(lowerCaseQuery)) {
                console.log("Valid place order request");
            }
            else if (validCancelOrderQueries.includes(lowerCaseQuery)) {
                console.log("Valid cancel order request");
            }
            else {
                errors.push({
                    statusCode: 400,
                    response: "Invalid parameters"
                });
            }
            if (Object.keys(customerData).length === 0 && customerData.constructor === Object) {
                errors.push({
                    statusCode: 400,
                    response: 'Empty payload'
                });
                // Handle the case when the payload is empty
            }
            //  customer ID
            if (lowerCaseQuery === "placeorder" && customerData.customerId && !customerData.customer) {
                console.log("Scenario -1 ");
                if (!customerData.customerId && (typeof customerData.customerId !== 'number' || customerData.customerId.toString().length >= 5) || customerData.customerId.toString() == " ") {
                    errors.push({
                        statusCode: 400,
                        response: "Customer ID should be a numeric string of exactly 5 characters."
                    });
                }
                if (!customerData.orders) {
                    console.log("this is order");
                    errors.push({
                        statusCode: 400,
                        response: "order object is required."
                    });
                }
                else {
                    for (const order of customerData.orders) {
                        if (!order.orderName || order.orderName.length > 25 || typeof order.orderName !== 'string') {
                            errors.push({
                                statusCode: 400,
                                response: "order name is missing or not valid"
                            });
                        }
                        if (!order.quantity || order.quantity.toString.length > 25 || order.quantity.toString() == " ")
                            errors.push({
                                statusCode: 400,
                                response: "quantity  data is missing or length excedds 5"
                            });
                        if (!order.vendorName || order.vendorName.length > 25 || typeof order.vendorName !== 'string')
                            errors.push({
                                statusCode: 400,
                                response: "vendorName is missing or exceeds 25 character"
                            });
                        if (!order.vendorMailAddress || order.vendorMailAddress == " " || order.vendorMailAddress.length > 25) {
                            errors.push({
                                statusCode: 400,
                                response: "vendorMailAddress is missing or exceeds 25 characters."
                            });
                        }
                        else {
                            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                            if (!emailRegex.test(order.vendorMailAddress)) {
                                errors.push({
                                    statusCode: 400,
                                    response: "Invalid email format."
                                });
                            }
                        }
                    }
                }
            }
            //customer object
            if (lowerCaseQuery === "placeorder" && customerData.customer && !customerData.customerId) {
                console.log("Scer -2");
                if (!customerData.customer.firstName || customerData.customer.firstName == " " || customerData.customer.firstName.length > 30 || typeof customerData.customer.firstName !== 'string') {
                    console.log("customerDatafirst", customerData);
                    errors.push({
                        statusCode: 400,
                        response: "First name is required or extends 30 characters."
                    });
                }
                if (customerData.customer.lastName) {
                    if (customerData.customer.lastName !== " " && customerData.customer.lastName.length > 30 || typeof customerData.customer.address.addressLine1 == 'string') {
                        console.log("lastname is valid ");
                    }
                    else
                        errors.push({
                            statusCode: 400,
                            response: "last name should not be empty or less than 25."
                        });
                }
                if (!customerData.customer.emailAddress || customerData.customer.emailAddress.length > 25 || customerData.customer.emailAddress == " ")
                    errors.push({
                        statusCode: 400,
                        response: "Email Address exceeds 25 characters Or it is empty)."
                    });
                else {
                    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                    if (!emailRegex.test(customerData.customer.emailAddress))
                        errors.push({
                            statusCode: 400,
                            response: "Invalid email format."
                        });
                }
                if (!customerData.customer.phoneNumber || customerData.customer.phoneNumber.toString().length !== 10)
                    errors.push({
                        statusCode: 400,
                        response: "phone number must need to have 10 digits."
                    });
                else {
                    const phoneRegex = /^\d{10}$/;
                    if (!phoneRegex.test(customerData.customer.phoneNumber.toString()))
                        errors.push({
                            statusCode: 400,
                            response: "invalid phone number."
                        });
                }
                if (!customerData.customer.address.addressLine1 || customerData.customer.address.addressLine1 == " " || customerData.customer.address.addressLine1.length > 50 || typeof customerData.customer.address.addressLine1 !== 'string')
                    errors.push({
                        statusCode: 400,
                        response: "Address data 1is missing or exceeds length 50."
                    });
                if (customerData.customer.address.addressLine2) {
                    if (customerData.customer.address.addressLine2 == " " || customerData.customer.address.addressLine2.length > 50 || typeof customerData.customer.address.addressLine2 !== 'string')
                        errors.push({
                            statusCode: 400,
                            response: "Address data 2 is missing or exceeds length 50."
                        });
                }
                if (!customerData.customer.address.city || customerData.customer.address.city == " " || typeof customerData.customer.address.city !== 'string' || customerData.customer.address.city.toString().length > 25)
                    errors.push({
                        statusCode: 400,
                        response: "city data is missing or exceeds 25 characters ."
                    });
                if (!customerData.customer.address.zipCode || customerData.customer.address.zipCode.toString().length > 25 || typeof customerData.customer.address.city !== 'string')
                    errors.push({
                        statusCode: 400,
                        response: "zipcode data is missing or exceeds 25 characters ."
                    });
                if ((!customerData.customer.address.state || customerData.customer.address.state == " " || customerData.customer.address.state.length > 25 || typeof customerData.customer.address.city !== 'string'))
                    errors.push({
                        statusCode: 400,
                        response: "State is required and need not to be empty or exceeds 25 characters )."
                    });
                if (!customerData.orders) {
                    console.log("this is order");
                    errors.push({
                        statusCode: 400,
                        response: "order object is required."
                    });
                }
                else {
                    for (const order of customerData.orders) {
                        if (!order.orderName || order.orderName.length > 25 || typeof order.orderName !== 'string') {
                            errors.push({
                                statusCode: 400,
                                response: "order name is missing or not valid"
                            });
                        }
                        if (!order.quantity || order.quantity.toString.length > 25 || order.quantity.toString() == " ")
                            errors.push({
                                statusCode: 400,
                                response: "quantity  data is missing or length excedds 5"
                            });
                        if (!order.vendorName || order.vendorName.length > 25 || typeof order.vendorName !== 'string')
                            errors.push({
                                statusCode: 400,
                                response: "vendorName is missing or exceeds 25 character"
                            });
                        if (!order.vendorMailAddress || order.vendorMailAddress == " " || order.vendorMailAddress.length > 25) {
                            errors.push({
                                statusCode: 400,
                                response: "vendorMailAddress is missing or exceeds 25 characters."
                            });
                        }
                        else {
                            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                            if (!emailRegex.test(order.vendorMailAddress)) {
                                errors.push({
                                    statusCode: 400,
                                    response: "Invalid email format."
                                });
                            }
                        }
                    }
                }
            }
            //both customer and id
            if (lowerCaseQuery === "placeorder" && customerData.customerId && customerData.customer) {
                console.log("Sce - 3");
                if (!customerData.customerId && (typeof customerData.customerId !== 'number' || customerData.customerId.toString().length >= 5) || customerData.customerId.toString() == " ") {
                    errors.push({
                        statusCode: 400,
                        response: "Customer ID should be a numeric string of exactly 5 characters."
                    });
                }
                if (!customerData.orders) {
                    console.log("this is order");
                    errors.push({
                        statusCode: 400,
                        response: "order object is required."
                    });
                }
                else {
                    if (customerData.customer.firstName) {
                        if (!customerData.customer.firstName || customerData.customer.firstName == " " || customerData.customer.firstName.length > 30 || typeof customerData.customer.firstName !== 'string') {
                            console.log("customerDatafirst", customerData);
                            errors.push({
                                statusCode: 400,
                                response: "First name is required or extends 30 characters."
                            });
                        }
                    }
                    if (customerData.customer.lastName) {
                        if (customerData.customer.lastName !== " " && customerData.customer.lastName.length > 30 || typeof customerData.customer.address.addressLine1 == 'string') {
                            console.log("lastname is valid ");
                        }
                        else
                            errors.push({
                                statusCode: 400,
                                response: "last name should not be empty or less than 25."
                            });
                    }
                    if (customerData.customer.emailAddress) {
                        if (!customerData.customer.emailAddress || customerData.customer.emailAddress.length > 25 || customerData.customer.emailAddress == " ")
                            errors.push({
                                statusCode: 400,
                                response: "Email Address exceeds 25 characters Or it is empty)."
                            });
                        else {
                            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                            if (!emailRegex.test(customerData.customer.emailAddress))
                                errors.push({
                                    statusCode: 400,
                                    response: "Invalid email format."
                                });
                        }
                    }
                    if (customerData.customer.phoneNumber) {
                        if (!customerData.customer.phoneNumber || customerData.customer.phoneNumber.toString().length !== 10)
                            errors.push({
                                statusCode: 400,
                                response: "phone number must need to have 10 digits."
                            });
                        else {
                            const phoneRegex = /^\d{10}$/;
                            if (!phoneRegex.test(customerData.customer.phoneNumber.toString()))
                                errors.push({
                                    statusCode: 400,
                                    response: "invalid phone number."
                                });
                        }
                    }
                    if (customerData.customer.address.addressLine1) {
                        if (!customerData.customer.address.addressLine1 || customerData.customer.address.addressLine1 == " " || customerData.customer.address.addressLine1.length > 50 || typeof customerData.customer.address.addressLine1 !== 'string')
                            errors.push({
                                statusCode: 400,
                                response: "Address data 1is missing or exceeds length 50."
                            });
                    }
                    if (customerData.customer.address.addressLine2) {
                        if (!customerData.customer.address.addressLine2) {
                            if (customerData.customer.address.addressLine2 == " " || customerData.customer.address.addressLine2.length > 50 || typeof customerData.customer.address.addressLine2 !== 'string')
                                errors.push({
                                    statusCode: 400,
                                    response: "Address data 2 is missing or exceeds length 50."
                                });
                        }
                    }
                    if (customerData.customer.address.city) {
                        if (!customerData.customer.address.city || customerData.customer.address.city == " " || typeof customerData.customer.address.city !== 'string' || customerData.customer.address.city.toString().length > 25)
                            errors.push({
                                statusCode: 400,
                                response: "city data is missing or exceeds 25 characters ."
                            });
                    }
                    if (customerData.customer.address.zipCode) {
                        if (!customerData.customer.address.zipCode || customerData.customer.address.zipCode.toString().length > 25 || typeof customerData.customer.address.city !== 'string')
                            errors.push({
                                statusCode: 400,
                                response: "zipcode data is missing or exceeds 25 characters ."
                            });
                    }
                    if (customerData.customer.address.state) {
                        if ((!customerData.customer.address.state || customerData.customer.address.state == " " || customerData.customer.address.state.length > 25 || typeof customerData.customer.address.city !== 'string'))
                            errors.push({
                                statusCode: 400,
                                response: "State is required and need not to be empty or exceeds 25 characters )."
                            });
                    }
                    for (const order of customerData.orders) {
                        if (!order.orderName || order.orderName.length > 25 || typeof order.orderName !== 'string') {
                            errors.push({
                                statusCode: 400,
                                response: "order name is missing or not valid"
                            });
                        }
                        if (!order.quantity || order.quantity.toString.length > 25 || order.quantity.toString() == " ")
                            errors.push({
                                statusCode: 400,
                                response: "quantity  data is missing or length excedds 5"
                            });
                        if (!order.vendorName || order.vendorName.length > 25 || typeof order.vendorName !== 'string')
                            errors.push({
                                statusCode: 400,
                                response: "vendorName is missing or exceeds 25 character"
                            });
                        if (!order.vendorMailAddress || order.vendorMailAddress == " " || order.vendorMailAddress.length > 25) {
                            errors.push({
                                statusCode: 400,
                                response: "vendorMailAddress is missing or exceeds 25 characters."
                            });
                        }
                        else {
                            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                            if (!emailRegex.test(order.vendorMailAddress)) {
                                errors.push({
                                    statusCode: 400,
                                    response: "Invalid email format."
                                });
                            }
                        }
                    }
                }
            }
            //cancel order
            if (lowerCaseQuery === "cancelorder") {
                if (!customerData.orders) {
                    console.log("this is order");
                    errors.push({
                        statusCode: 400,
                        response: "order object is required."
                    });
                }
                else {
                    if (!customerData.customerId && (typeof customerData.customerId !== 'number' || customerData.customerId.toString().length >= 5) || customerData.customerId.toString() == " ") {
                        errors.push({
                            statusCode: 400,
                            response: "Customer ID should be a numeric string of exactly 5 characters."
                        });
                    }
                    for (const order of customerData.orders) {
                        if (!order.orderName || order.orderName.length > 25 || typeof order.orderName !== 'string') {
                            errors.push({
                                statusCode: 400,
                                response: "order name is missing or not valid"
                            });
                        }
                        if (!order.orderId || order.orderId.toPrecision().length > 25 || typeof order.orderName !== 'string') {
                            errors.push({
                                statusCode: 400,
                                response: "order ID is missing or not valid"
                            });
                        }
                        if (!order.quantity || order.quantity.toString.length > 25 || order.quantity.toString() == " ")
                            errors.push({
                                statusCode: 400,
                                response: "quantity  data is missing or length excedds 5"
                            });
                        if (!order.vendorName || order.vendorName.length > 25 || typeof order.vendorName !== 'string')
                            errors.push({
                                statusCode: 400,
                                response: "vendorName is missing or exceeds 25 character"
                            });
                        if (!order.vendorMailAddress || order.vendorMailAddress == " " || order.vendorMailAddress.length > 25) {
                            errors.push({
                                statusCode: 400,
                                response: "vendorMailAddress is missing or exceeds 25 characters."
                            });
                        }
                        else {
                            const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                            if (!emailRegex.test(order.vendorMailAddress)) {
                                errors.push({
                                    statusCode: 400,
                                    response: "Invalid email format."
                                });
                            }
                        }
                    }
                    if (!customerData.cancellationReason || customerData.cancellationReason.length > 100 || typeof customerData.cancellationReason !== 'string') {
                        errors.push({
                            statusCode: 400,
                            response: "cancellationReason is missing."
                        });
                    }
                }
            }
            return Promise.resolve(errors);
        });
    }
}
exports.Validator = Validator;
