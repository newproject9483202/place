"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const OrderStatusCont_1 = require("../controller/OrderStatusCont");
//import { Customer, validateError } from '../module/OrderInterface';
const app = (0, express_1.default)();
const port = 8080;
app.use(express_1.default.json());
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});
app.listen(port, () => {
    console.log("Server is running on port", port);
});
app.post("/postcustomer", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        OrderStatusCont_1.ValidateClass.validateOrder(req, res);
        console.log("body", req.body);
        console.log("query", req.query.order_type);
    }
    catch (error) {
        console.error("Error occurred:", error);
        res.status(500).send("An error occurred while processing the request.");
    }
}));
