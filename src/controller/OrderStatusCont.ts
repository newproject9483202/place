import { Validator } from '../validator/OrderStatusValidate';
import express, { Request, Response } from 'express';
import { OrderStatusService } from '../service/OrderStatusService'; // Import the ServiceFunction from OrderStatusService.ts
import {  validateError } from '../module/OrderInterface';
import { log } from 'console';
import { loadavg } from 'os';
export class ValidateClass {
    
    static async validateOrder(req: Request, res: Response) {
        try {
            const query= req.query.order_type as string;
            console.log("coming to validate");
            
            const errors: validateError[] = await Validator.requestValidation(req.body,query);
            console.log("error response for validator", errors);    
            console.log("controller body", req.body);
            console.log("query", req.query.order_type);


            if (errors.length === 0) {
               const query= req.query.order_type as string
          // If the errors array is empty, call the ServiceFunction in OrderStatusService.ts
                const serviceresponse:any = await OrderStatusService.ServiceFunction(req.body, query);
                //res.status(200).send(serviceresponse);
                    res.status(serviceresponse.statusCode).send(serviceresponse);
                
            } else {
                // If errors are present, throw an error to the front-end
                res.status(400).json({ error: "Validation errors found", errors: errors });
            }
            
        } catch (error: any) {
            console.error("Error occurred:", error);
            res.status(500).send({
                statusCode: 500,
                Response: error.message || "internal server error"
            });
        }
    }
}

