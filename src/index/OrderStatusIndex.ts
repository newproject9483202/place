import express, { Request, Response } from 'express';
import { ValidateClass } from '../controller/OrderStatusCont';
//import { Customer, validateError } from '../module/OrderInterface';


const app = express();
const port = 8080;

app.use(express.json());

app.use((req:Request, res: Response, next: Function) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.listen(port, () => {
  console.log("Server is running on port", port);
});

app.post("/postcustomer", async (req: Request, res: Response) => {
  try {

       ValidateClass.validateOrder(req, res);
    console.log("body", req.body);
    console.log("query", req.query.order_type);
  } catch (error) {
    console.error("Error occurred:", error);
    res.status(500).send("An error occurred while processing the request.");
  }
});






