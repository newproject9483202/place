//import { Request, Response } from 'express';
import axios from 'axios';
import { Pool } from 'pg';
import { CustomerDTO, OrderStatusResponseDTO, OrderPayloadDTO, Order } from '../service/Dto/CustomerOrderDto';
//import { Customer, validateError,re } from '../module/OrderInterface';


const pool = new Pool({
  host: 'localhost',
  user: 'postgres',
  password: 'admin123',
  database: 'Cuatomer_details(type script)',
  port: 5432,
});


export class repo {
  static async isCustomerExist(req: CustomerDTO, query: string): Promise<OrderStatusResponseDTO> {
    console.log("hi customer id and order");
    const requestBody: CustomerDTO = req;

    try {
      const customerId = requestBody.customerId;
      const orders = requestBody.orders;
      const insertedOrders: { orderId: number; customerId: number }[] = [];

      for (const order of orders) {
        const { orderName, quantity, vendorName, vendorMailAddress } = order;
        console.log("orders fin",orderName)

        const insertQuery = {
          text: `INSERT INTO OrderTable (customer_id, order_name, quantity, vendor_name, vendor_mail_address) VALUES ($1, $2, $3, $4, $5) RETURNING order_id`,
          values: [customerId, orderName, quantity, vendorName, vendorMailAddress],
        };

        const result = await pool.query(insertQuery);
        const lastOrderId = result.rows[0].order_id;
        console.log("lastOrderId", lastOrderId);

        insertedOrders.push({ orderId: lastOrderId, customerId: customerId });
      }

      // Make the external API call
      const emailAddress = requestBody.orders[0].vendorMailAddress;
      const payload = {
        emailAddress: emailAddress,
        subject: query,
        content: orders.map((order,index) => {
          return {
            orderId: insertedOrders[index].orderId,
            orderName: order.orderName,
            quantity: order.quantity,
            vendorName: order.vendorName,
          };
        }),
        orderIDs: insertedOrders.map(({ orderId }) => orderId), // Add orderIDs to the payload
      };

      // Assuming you have a makeExternalAPICall function
      const resultAtExternal = await this.makeExternalAPICall(req, query, payload);
      console.log('The result for external API:', resultAtExternal);

      const orderIdsArray = insertedOrders.map(({ orderId }) => orderId);
      const responseMessage = `Orders placed successfully for given orders `;

      const response: OrderStatusResponseDTO = {
        statusCode: 200,
        statusMessage: 'Success',
        result: {
          response: responseMessage,
          orderIDs: orderIdsArray,
        },
      };
      return response;
    } catch (error: any) {
      console.error('Error processing order data:', error);
      const response: OrderStatusResponseDTO = {
        statusCode: 500,
        statusMessage: 'Error',
        result: {
          response: error.message,
          orderIDs: [],
        },
      };
      return response;
    }
  }

  static async makeExternalAPICall(req: CustomerDTO, query: string, payload: OrderPayloadDTO) {
    console.log("hi this is external API call")
    const apiUrl = 'http://localhost:8081/sendmail';
    console.log('externalAPIURLfinal', apiUrl);

    let retries = 3; // Number of retries

    while (retries > 0) {
      try {
        const response = await axios.post(apiUrl, payload);
        console.log('response from external', response);
        console.log('Mail sent to the external vendor:', response.data);
        return; // Set success to true to break the retry loop
      } catch (error: any) {
        console.error('Error making external API call:', error.response?.data || error.message);
        retries--; // Decrement the number of retries remaining
        if (retries === 0) {
          console.log("here in external API's scope")
          // If no retries left, return the error response
          throw {
            status: 500,
            response: 'Error making external API call.'
          }
        }
      }
      console.log('Retries remaining:', retries);
    }
    console.log('Retries remaining:', retries);
   }









  //customer object

  static async isCustomerObject(req: CustomerDTO, query:string): Promise<OrderStatusResponseDTO> {
    console.log("here is customer object function");
    try {
      const requestBody = req;

      const customer:any = requestBody.customer;
      const orders = requestBody.orders || [];

      const customerValues = [
        customer.firstName,
        customer.lastName,
        customer.emailAddress,
        customer.phoneNumber,
        customer.address?.addressLine1,
        customer.address?.addressLine2,
        customer.address?.city,
        customer.address?.state,
        customer.address?.zipCode,
      ];

      const customerInsertQuery = await pool.query(
        `INSERT INTO Customer (first_name, last_name, email_address, phone_number, address_line1, address_line2, city, state, zip_code) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING customer_id`,
        customerValues
      );

      const customerId = customerInsertQuery.rows[0].customer_id;

      const orderIdsArray: number[] = [];

      for (const order of orders) {
        const orderValues = [
          customerId,
          order.orderName,
          order.quantity,
          order.vendorName,
          order.vendorMailAddress,
        ];

        const orderInsertQueryResult = await pool.query(
          `INSERT INTO OrderTable (customer_id, order_name, quantity, vendor_name, vendor_mail_Address) VALUES ($1, $2, $3, $4, $5) RETURNING order_id`,
          orderValues
        );

        const orderId = orderInsertQueryResult.rows[0].order_id;
        console.log("orderId", orderId);

        // Check if orderId is a valid number before adding to the array
        if (!isNaN(orderId)) {
          orderIdsArray.push(orderId);
        }
      }

      const emailAddress = requestBody.orders[0].vendorMailAddress;
      const payload: OrderPayloadDTO = {
        emailAddress: emailAddress,
        subject: query,
        content: orders.map((order: Order,index) => {
          return {
            orderId: orderIdsArray[index],
            orderName: order.orderName,
            quantity: order.quantity,
            vendorName: order.vendorName,
          };
        }),
        
      };

      // Make the external API call
      await this.makeExternalAPICall(req, query, payload);

      // Send a response only after the external API call is complete
      const responseMessage = `The order has been placed for orders.`;

      const response: OrderStatusResponseDTO = {
        statusCode: 200,
        statusMessage: 'Success',
        result: {
          response: responseMessage,
          orderIDs: orderIdsArray,
          customerId:customerId
        },
      };
      return response;
    } catch (e: any) {
      console.error('Error processing order data:', e);
      throw {
        status: 500,
        message: e.message,
      };
    }
  }



  static async BothCustomerid(req: CustomerDTO, query:string): Promise<OrderStatusResponseDTO> {
    console.log('here in function BothCustomerid');
    const requestBody = req;
    const queryparm = query;
    console.log('queryparm', queryparm);

    const emailAddress = requestBody.orders[0].vendorMailAddress;
    console.log('both emailaddress', emailAddress);

    const customerId = requestBody.customerId;
    const customer = requestBody.customer;
    const orders = requestBody.orders;

    if (customerId && customer) {
      // Check if customerId exists in the database
      const customerExist = await pool.query(`SELECT * FROM Customer WHERE customer_id = $1`, [customerId]);
      //console.log("customerExist",customerExist)

      if (customerExist.rows.length > 0) {
        // Customer exists, update the details
        const existingCustomer = await pool.query(`SELECT * FROM Customer WHERE phone_number = $1 AND customer_id != $2`, [customer.phoneNumber, customerId]);

    if (existingCustomer.rows.length > 0) {
      // Phone number already exists for another customer, throw an error
      return {
        statusCode: 400,
        statusMessage: 'Bad Request',
        result: {
          response: 'Phone number already exists.',
  
        },
      };
    }


        // Build the SET clause for the SQL query dynamically based on provided fields
        const setClause: string[] = [];
        const updateValues = [];

        if (customer.firstName) {
          setClause.push(`first_name = $${updateValues.push(customer.firstName)}`);
        }
        if (customer.lastName) {
          setClause.push(`last_name = $${updateValues.push(customer.lastName)}`);
        }
        if (customer.emailAddress) {
          setClause.push(`email_address = $${updateValues.push(customer.emailAddress)}`);
        }
        if (customer.phoneNumber) {
          setClause.push(`phone_number = $${updateValues.push(customer.phoneNumber)}`);
        }
        if (customer.address) {
          const address = customer.address;
          if (address.addressLine1) {
            setClause.push(`address_line1 = $${updateValues.push(address.addressLine1)}`);
          }
          if (address.addressLine2) {
            setClause.push(`address_line2 = $${updateValues.push(address.addressLine2)}`);
          }
          if (address.city) {
            setClause.push(`city = $${updateValues.push(address.city)}`);
          }
          if (address.state) {
            setClause.push(`state = $${updateValues.push(address.state)}`);
          }
          if (address.zipCode) {
            setClause.push(`zip_code = $${updateValues.push(address.zipCode)}`);
          }
        }

        // Update only if there are fields to update
        if (setClause.length > 0) {
          try {
            const updateCustomer = await pool.query(
              `UPDATE Customer SET ${setClause.join(', ')} WHERE customer_id = $${updateValues.push(customerId)}`,
              updateValues
            );

        

            console.log("Updated Customer:", updateCustomer.rows[0]);

            console.log("updateCustomer", updateCustomer)
            console.log("setClause", setClause);
            console.log("updateValues", updateValues)
            console.log('Customer details updated:', updateCustomer);
          } catch (error:any) {
            
            console.error('Error updating customer data:', error);
      
      
            throw error;
          }
        }

        // Insert orders associated with the customer
        const orderIdsArray: number[] = [];
        if (orders && orders.length > 0) {
          for (let order of orders) {
            const orderValues = [
              customerId,
              order.orderName,
              order.quantity,
              order.vendorName,
              order.vendorMailAddress,
            ];
            try {
              const insertOrder = await pool.query(
                `INSERT INTO OrderTable (customer_id, order_name, quantity, vendor_name, vendor_mail_address) VALUES ($1, $2, $3, $4, $5) RETURNING order_id`,
                orderValues
              );
              const insertedOrderId = insertOrder.rows[0].order_id;
              orderIdsArray.push(insertedOrderId);

              console.log('Order details inserted:', insertOrder.rows);
              console.log('Order details inserted:', insertOrder.rows);
            } catch (error) {
              console.error('Error inserting order data:', error);
              throw error;
            }
          }
        }

        // Prepare and send the payload for the external API call
        const payload: OrderPayloadDTO = {
          emailAddress: emailAddress,
          subject: query,
          content: orders.map((order: Order,index) => {
            return {
               orderId: orderIdsArray[index], 
              orderName: order.orderName,
              quantity: order.quantity,
              vendorName: order.vendorName,
            };
          }),
        };

        console.log('emailAddress', payload.emailAddress);
        console.log('query', payload.subject);

        await this.makeExternalAPICall(req, query, payload);

        // Return a proper response
        return {
          statusCode: 200,
          statusMessage: 'Success',
          result: {
            response: `The order has been placed for the  orders.`,
            orderIDs: orderIdsArray,
          },

        }
      }


      else {
        // Customer does not exist, throw an error
        throw new Error('Invalid customer_id. Customer does not exist.');
      }
    } else {
      throw {
        statusCode: 400,
        statusMessage: 'Bad Request',
        result: {
          response: 'customerId or customer object missing. Cannot proceed.',
          orderIDs: null,
        },
      }
    }
  }



//cancel order

static async CancelOrder(req: CustomerDTO, query: string): Promise<OrderStatusResponseDTO> {
  const { customerId, orders, cancellationReason } = req;

  if (!(customerId && orders && cancellationReason)) {
    return {
      statusCode: 400,
      statusMessage: "fail",
      result: 'Data is missing.',
    };
  }

  try {
    const validOrders = [];
    const validOrderss=[];
    const invalidOrders = [];

    for (const order of orders) {
      const { orderId } = order;

      const orderCheck = await pool.query(
        `SELECT * FROM OrderTable WHERE customer_id = $1 AND order_id = $2`,
        [customerId, orderId]
      );

      if (orderCheck.rows.length > 0) {
        const updateCancellation = await pool.query(
          `UPDATE OrderTable SET cancellation_reason = $1 WHERE customer_id = $2 AND order_id = $3`,
          [cancellationReason, customerId, orderId]
        );

        console.log(`Order (${orderId}) cancellation reason updated for customer (${customerId}):`, updateCancellation);

        validOrders.push({
          orderId: parseInt(order.orderId),
          orderName: order.orderName,
          quantity: order.quantity,
          vendorName: order.vendorName,
          cancellationReason:req.cancellationReason
        });
      
      validOrderss.push({
        orderId: parseInt(order.orderId),
      }) 
    }else {
        console.log(`Customer (${customerId}) is not associated with order (${orderId})`);
        invalidOrders.push({
          orderId: parseInt(order.orderId),
          // orderName: order.orderName,
          // quantity: order.quantity,
          // vendorName: order.vendorName,
          reason: `Customer is not associated with this order.`,
        });
      }
    }

    if (validOrders.length > 0) {
      const result = {
        validOrderss,
        invalidOrders: invalidOrders.length > 0 ? invalidOrders : undefined,
      };

      const emailAddress = req.orders[0].vendorMailAddress;
      const payload = {
        emailAddress: emailAddress,
        subject: query,
        content: validOrders,
      };

      try {
        await this.makeExternalAPICall(req, query, payload);
        
        // Check if there are both valid and invalid orders
        const statusCode = invalidOrders.length > 0 ? 400 : 200;
        const statusMessage = invalidOrders.length > 0 ? "failure" : "success";

        return {
          statusCode,
          statusMessage,
          result,
        };
      } catch (error) {
        console.error('Error making external API call:', error);
        throw {
          statusCode: 500,
          message: 'Error making external API call.',
        };
      }
    } else {
      return {
        statusCode: 400,
        statusMessage: "Failure",
        result: 'No valid orders provided for cancellation.',
      };
    }
  } catch (error: any) {
    console.error('Error cancelling order:', error);
    throw {
      statusCode: 500,
      message: 'Error cancelling order: ' + error.message,
    };
  }
}
}