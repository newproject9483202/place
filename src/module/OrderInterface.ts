export interface Customer {
    customerId: number;
    customer: {
        firstName: string;
        lastName: string;
        emailAddress: string;
        phoneNumber: number;
        address: {
            addressLine1: string;
            addressLine2: string;
            city: string;
            state: string;
            zipCode: number;
        };
    },
    orders: [{
        orderId: number;
        orderName: string;
        quantity: number;
        vendorName: string;
        vendorMailAddress: string;
    }],
    cancellationReason?: string
}

export interface validateError {
    statusCode: number,
    response: string
}
export interface responses{
    response:"string"
}