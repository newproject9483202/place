

export interface CustomerDTO {
    customerId: number;
    customer?: {
        firstName: string;
        lastName?: string;
        emailAddress: string;
        phoneNumber: number;
        address: {
            addressLine1: string;
            addressLine2: string;
            city: string;
            state: string;
            zipCode: number;
        };
    };
    orders: OrderDTO[];
    cancellationReason?: string;
}

export interface OrderDTO {
    orderId: string;
    orderName: string;
    quantity: number;
    vendorName: string;
    vendorMailAddress: string;
}

export interface OrderStatusResponseDTO {
    statusCode:number,
    statusMessage:string,
    result:{}
    message?: string;
}
//  export interface OrderStatusResponseDTO {
//     status: number;
//     message: string;
//     orderid:[]
//   }




export interface OrderPayloadDTO {
    emailAddress: string;
    subject: string;
    content: OrderContentDTO[];
  
}


// Assuming an OrderContentDTO definition
export interface OrderContentDTO {
    orderId: number;
    orderName: string;
    quantity: number;
    vendorName: string;
    vendorMailAddress?: string;
    cancellationReason?: string;
}

export interface Order {
    orderId: string;
    orderName: string;
    quantity: number;
    vendorName: string;
    
};
