import { repo } from '../repo/OrderStatusRepository';
import { CustomerDTO, OrderStatusResponseDTO } from '../service/Dto/CustomerOrderDto';
import { Customer, responses } from '../module/OrderInterface';
import { Response } from 'express';

export class OrderStatusService {
    static async ServiceFunction(reqst: CustomerDTO, query: string) {
        try {
            // Convert the query parameter to lowercase for case-insensitive comparison
            const lowerCaseQuery = query.trim().toLowerCase().replace(/[^a-z]/g, '');

            const customerDto: CustomerDTO = {
                customerId: reqst.customerId,
                orders: reqst.orders
            };

            if (lowerCaseQuery === "placeorder" || lowerCaseQuery === "place_order"){
                if (reqst.customerId && !reqst.customer) {
                    console.log("customer_id");
                    const customerExist = await repo.isCustomerExist(reqst, lowerCaseQuery);
                    return customerExist;
                } else if (reqst.customer && !reqst.customerId) {
                    console.log("customer object");
                    const customerObject = await repo.isCustomerObject(reqst, lowerCaseQuery);
                    return customerObject;
                } else {
                    const customerBoth = await repo.BothCustomerid(reqst, lowerCaseQuery);
                    return customerBoth;
                }
            } else if (lowerCaseQuery === "cancelorder"||lowerCaseQuery === "cancel_order") {
                const cancelOrder = await repo.CancelOrder(reqst, lowerCaseQuery);
                console.log("result at service from repo:", cancelOrder);
                return cancelOrder;
            } else {
                // Invalid query parameter
                throw {
                    statusCode: 400,
                    statusMessage: 'Bad Request',
                    result: {
                        response: 'Invalid parameter',
                    },
                };
            }
        } catch (error: any) {
            throw error;
        }
    }
}
